# dht22-datalogger
Features:
 - STM32F103C8T6 uC
 - SD card over SPI for logging data
 - [FatFs R0.13a](http://elm-chan.org/fsw/ff/00index_e.html)
 - CSV output file
 - DHT22 temperature and humidity sensor
 - UART for logging progress & errors

Changes from upstream FatFs:
 - I had to rename `ffconf.h` to `ffconf_template.h`
 - Provide my custom `ffconf.h` in `Inc/`
