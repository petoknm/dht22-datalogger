#include "diskio.h"
#include "stm32f1xx_hal.h"

extern SPI_HandleTypeDef hspi2;
#define FAT_SD_SPI hspi2

#define CAP_VER2_00	(1<<0)
#define CAP_SDHC	(1<<1)

struct hwif {
	int initialized;
	int sectors;
	int erase_sectors;
	int capabilities;
};
typedef struct hwif hwif;

enum sd_speed { SD_SPEED_INVALID, SD_SPEED_400KHZ, SD_SPEED_25MHZ };

int hwif_init(hwif* hw);
int sd_read(hwif* hw, uint32_t address, uint8_t *buf);
int sd_write(hwif* hw, uint32_t address,const uint8_t *buf);

#define sd_get_r3 sd_get_r7
