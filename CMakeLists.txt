project(dht22_datalogger C)
cmake_minimum_required(VERSION 3.5)

enable_language(ASM)

set(C_SOURCES
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio_ex.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash_ex.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_flash.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_tim_ex.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_dma.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_pwr.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_gpio.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_cortex.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_rcc_ex.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_uart.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_spi_ex.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_spi.c
    Drivers/STM32F1xx_HAL_Driver/Src/stm32f1xx_hal_i2c.c

    Drivers/FatFs/source/ff.c

    Drivers/DHT22/src/dht22.c

    Drivers/DS1307/src/ds1307.c

    Src/stm32f1xx_hal_msp.c
    Src/stm32f1xx_it.c
    Src/main.c
    Src/system_stm32f1xx.c
    Src/datalogger.c
    Src/diskio.c
    Src/fat_sd_spi.c
)

set(ASM_SOURCES
    startup_stm32f103xb.s
)

include_directories(
    Inc
    Drivers/STM32F1xx_HAL_Driver/Inc
    Drivers/STM32F1xx_HAL_Driver/Inc/Legacy
    Drivers/CMSIS/Device/ST/STM32F1xx/Include
    Drivers/CMSIS/Include

    Drivers/DHT22/inc
    Drivers/DS1307/inc
    Drivers/FatFs/source
)

add_definitions(
    -DUSE_HAL_DRIVER
    -DSTM32F103xB
    -DSTM32F1
)

set(MCU "-mcpu=cortex-m3 -mthumb")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${MCU} -Wall -fdata-sections -ffunction-sections -flto")
set(CMAKE_ASM_FLAGS "${CMAKE_C_FLAGS} ${MCU} -Wall -fdata-sections -ffunction-sections -flto")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -flto --specs=nano.specs -Wl,--gc-sections -T${CMAKE_CURRENT_SOURCE_DIR}/STM32F103C8Tx_FLASH.ld")

add_executable(dht22_datalogger.elf ${C_SOURCES} ${ASM_SOURCES})
set_target_properties(dht22_datalogger.elf PROPERTIES LINK_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/STM32F103C8Tx_FLASH.ld")

add_custom_command(TARGET dht22_datalogger.elf POST_BUILD COMMAND arm-none-eabi-size --format=berkeley dht22_datalogger.elf)
add_custom_command(TARGET dht22_datalogger.elf POST_BUILD COMMAND arm-none-eabi-objcopy -O binary -S dht22_datalogger.elf dht22_datalogger.bin)
