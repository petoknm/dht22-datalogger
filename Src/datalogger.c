#include "stm32f1xx_hal.h"
#include "dht22.h"
#include "ds1307.h"
#include "ff.h"
#include <string.h>

extern TIM_HandleTypeDef  htim3;
extern UART_HandleTypeDef huart1;
extern I2C_HandleTypeDef hi2c1;

#define print(msg) HAL_UART_Transmit(&huart1, (uint8_t*)msg, strlen(msg), 100)
#define BUTTON GPIOA, GPIO_PIN_5
#define LED GPIOC, GPIO_PIN_13

dht22 dht; // <-- used in the interrupt handler (*_it.c)
ds1307 rtc; // <-- used in the low level fatfs disk access (diskio.c)
static FATFS FatFs;
static FIL file;

static void error(const char* desc, int v) {
    char msg[64];
    snprintf(msg, 64, "Error: %s (%d)\r\n", desc, v);
    while(true) {
        print(msg);
        HAL_GPIO_WritePin(LED, GPIO_PIN_RESET);
        HAL_Delay(100);
        HAL_GPIO_WritePin(LED, GPIO_PIN_SET);
        HAL_Delay(100);
    };
}

static void dht_init() {
    dht22_config dht_config;

    dht_config.gpio_pin      = GPIO_PIN_6;
    dht_config.gpio_port     = GPIOA;
    dht_config.timer         = &htim3;
    dht_config.timer_channel = TIM_CHANNEL_1;

    DHT22_RESULT r;

    if ((r = dht22_init(&dht_config, &dht)) != DHT22_OK) error("dht22_init", r);
}

static void rtc_init() {
    ds1307_init(&rtc, &hi2c1);

    ds1307_datetime datetime = ds1307_get_datetime(&rtc);

    char msg[64];
    snprintf(msg, 64, "RTC initialized: %.4d-%.2d-%.2dT%.2d:%.2d:%.2d\r\n", datetime.year, datetime.month, datetime.date, datetime.hours, datetime.minutes, datetime.seconds);
    print(msg);
}

static void rtc_setup() {
    uint8_t data[8];

    print("Waiting for RTC settings...\r\n");

    if (HAL_UART_Receive(&huart1, data, 8, 60*1000) != HAL_OK) {
        return;
    }

    ds1307_datetime datetime = {
        .seconds = data[0],
        .minutes = data[1],
        .hours = data[2],
        .day = data[3],
        .date = data[4],
        .month = data[5],
        .year = data[6] << 8 | data[7]
    };

    char msg[64];
    snprintf(msg, 64, "Setting date to %.4d-%.2d-%.2dT%.2d:%.2d:%.2d\r\n", datetime.year, datetime.month, datetime.date, datetime.hours, datetime.minutes, datetime.seconds);
    print(msg);

    ds1307_set_datetime(&rtc, datetime);
}

static void sd_init() {
    FRESULT fr;

    fr = f_mount(&FatFs, "", 0);
    if (fr) error("f_mount", fr);

    fr = f_open(&file, "data.csv", FA_WRITE | FA_OPEN_APPEND);
    if (fr) error("f_open", fr);
}

static void sd_write(int16_t temp, uint16_t hum) {
    char line[128];
    ds1307_datetime datetime = ds1307_get_datetime(&rtc);
    snprintf(line, 64, "%.4d-%.2d-%.2dT%.2d:%.2d:%.2d, %d.%d, %d.%d\n", datetime.year, datetime.month, datetime.date, datetime.hours, datetime.minutes, datetime.seconds, temp/10, temp%10, hum/10, hum%10);

    FRESULT fr;

    unsigned int bytes_writen;
    fr = f_write(&file, line, strlen(line), &bytes_writen);
    if (fr) error("f_write", fr);
    if (bytes_writen != strlen(line)) error("bytes_writen != strlen(line)", bytes_writen);

    fr = f_sync(&file);
    if (fr) error("f_sync", fr);
}

static void sd_close() {
    FRESULT fr;

    fr = f_close(&file);
    if (fr) error("f_close", fr);

    fr = f_mount(0, "", 0);
    if (fr) error("f_unmount", fr);
}

void start_log() {

    print("Starting datalogging...\r\n");
    HAL_Delay(100);

    dht_init();
    sd_init();
    rtc_init();

    if (!HAL_GPIO_ReadPin(BUTTON)) {
        rtc_setup();
    }

    DHT22_RESULT r;

    while(HAL_GPIO_ReadPin(BUTTON)) {
        if ((r = dht22_read_data(&dht)) != DHT22_OK) error("dht22_read_data", r);

        sd_write(dht.temp, dht.hum);

        HAL_GPIO_WritePin(LED, GPIO_PIN_RESET);
        HAL_Delay(100);
        HAL_GPIO_WritePin(LED, GPIO_PIN_SET);
        HAL_Delay(10000);
    }

    sd_close();
    print("Done!\r\n");

    for (int i=0; i<10; i++){
        HAL_GPIO_WritePin(LED, GPIO_PIN_RESET);
        HAL_Delay(100);
        HAL_GPIO_WritePin(LED, GPIO_PIN_SET);
        HAL_Delay(100);
    }
}
